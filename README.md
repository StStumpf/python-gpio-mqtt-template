# Backend Template DHBW Mosbach Softwareprojekt

## Abhängigkeiten

- Raspberry Pi installiert nach Vorgabe [Vorgabe](https://gitlab.com/ingoha/dhbw-raspi)
- [Python](https://www.python.org/) 3.6 und höher
- Raspberry Pi GPIO package
- LEDs und Widerstände

## Hardware

Angeschlossen sind die LEDs wie folgt:

### LED grün

```
GPIO24 > 220 Ohm Widerstand > LED > GND
```

### LED blau

```
GPIO23 > 220 Ohm Widerstand > LED > GND
```

## Installation aller benötigten Python Pakete

Dies kann entweder in einem lokalen Environement (siehe [venv](https://docs.python.org/3/library/venv.html)) oder global installiert werden.

Für die Installation kann pip verwendet werden.

```
pip3 install -U -r requirements.txt
```

Alternativ können alle Pakete auch von Hand installiert werden. Diese finden sich in der Datei `requirements.txt`.

## Starten der Applikation

Zum Starten der Applikation kann der Befehl:

```
python3 main.py
```

verwendet werden. Je nach Installation kann es vorkommen das Python Version 3 unter `python` zu finden ist und nicht unter `python3`.
